// Iteración #2: Condicionales avanzados

// Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados y añade la propiedad isApproved a trlsue o fae en consecuencia. Una vez lo tengas compruébalo con un console.log
// Puedes usar este array para probar tu función:

const alumns = [{
        name: 'Pepe Viruela',
        T1: false,
        T2: false,
        T3: true
    },
    {
        name: 'Lucia Aranda',
        T1: true,
        T2: false,
        T3: true
    },
    {
        name: 'Juan Miranda',
        T1: false,
        T2: true,
        T3: true
    },
    {
        name: 'Alfredo Blanco',
        T1: false,
        T2: false,
        T3: false
    },
    {
        name: 'Raquel Benito',
        T1: true,
        T2: true,
        T3: true
    }
]

for (let key of alumns) {
    let increment = 0;
    let isApproved = false;

    for (value in key) {
        if(key[value] == true){
            increment++;
        }

        increment >= 2 ? key.isApproved = true : key.isApproved = false;
    }
}

console.log(alumns);

