// Iteración #6: Mixed For...of e includes

// Usa un bucle for...of para recorrer todos los juguetes y elimina los que incluyan la palabra gato. Recuerda que puedes usar la función .includes() para comprobarlo.Puedes usar este array:

const toys = [{
        id: 5,
        name: 'Buzz MyYear'
    },
    {
        id: 11,
        name: 'Action Woman'
    },
    {
        id: 23,
        name: 'Barbie Man'
    },
    {
        id: 40,
        name: 'El gato con Guantes'
    },
    {
        id: 40,
        name: 'El gato felix'
    }
]
const i = [];
for (key of toys){    
    if(key.name.includes('gato')){
        i.push(toys.indexOf(key));
    }
}

for (x = i.length-1; x >=0; x--){
    toys.splice(i[x],1);
}

console.log(toys);